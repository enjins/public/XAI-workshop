
import joblib
import pandas as pd
import numpy as np
import json
import shap
from azureml.core.model import Model

def init():
    global model
    ### ADD EXPLAINER

    # Retrieve the path to the model file using the model name
    model_path = Model.get_model_path('<YOUR_MODEL_NAME>') #
    ### ADD EXPLAINER

    # Get model
    model = joblib.load(model_path)
    ### ADD EXPLAINER


def run(raw_data):
    data = json.loads(raw_data)['data']

    # Make prediction
    predictions = model.predict(data)
    
    # Setup for inferencing explanations
    ### ADD EXPLAINER
    
    # You can return any data type as long as it is JSON-serializable
    return {'predictions': predictions.tolist()
            ### ADD EXPLAINER
            }