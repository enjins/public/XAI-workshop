import joblib
import pandas as pd
import numpy as np
import json
import shap
from azureml.core.model import Model

def init():
    global model
    global explainer

    # Retrieve the path to the model file using the model name
    model_path = Model.get_model_path('my_sklearn_model_test')
    explainer_path = Model.get_model_path('my_sklearn_explainer_test')

    # Get model
    model = joblib.load(model_path)
    explainer = joblib.load(explainer_path)


def run(raw_data):
    data = json.loads(raw_data)['data']

    # Make prediction
    predictions = model.predict(data)
    
    # Setup for inferencing explanations
    #shap_values = scoring_explainer.shap_values(np.array(data))
    shap_values = scoring_explainer.shap_values(np.array(data))
    
    # You can return any data type as long as it is JSON-serializable
    return {'predictions': predictions.tolist(),
            'shap_values': [shap.tolist() for shap in shap_values]
            }
