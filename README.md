# XAI Workshop

## FILES
The files in this repository are all you need for the hands-on part of the XAI workshop.
Besides this readme file, you will find the following:

- directory named "Complete" : Here you can find 3 files:
    - "xai-workshop-complete.ipynb" : this notebook contains ready to run code that trains and deploys a model and an explainer. This notebook can be used as a cheatsheet during the workshop.
    - "score.py" : this python script is the entry script needed when creating the config for inference. This is used by the notebook and can also be used as a cheatsheet.
    - "phishing_dataset.csv" : the dataset containing observations on websites. More information on the dataset can be found on https://archive.ics.uci.edu/ml/datasets/Website+Phishing#
- directory named "Hands-On" : Here you can find 3 files:
    - "xai-workshop-handson.ipynb" :  this notebook contains almost ready to use code. It still needs some input, which we will create during the workshop.
    - "score.py" : this python script is the entry script needed when creating the config for inference. However, as of now, it only contains the code for the model, the code for the explainer needs to be added.
    -  "phishing_dataset.csv" : this is the same dataset as in the other directory.

## HANDS ON TUTORIAL
- Part 1: 
    - Launch Azure ML using the central Sandbox env (see instructions in Teams chat).
    - Clone this repo in your Azure ML workspace using the available compute.
    - Go to Notebooks, then to your own folder. Here you’ll find a notebook with the code to train a model and an explainer. 
    - Feel free to play around with the code a bit and make sure to run the “train model and explainer” part. This gives you the needed artefacts to deploy a model and explainer later on.
    - After training the model and explainer, we use a test instance to generate local SHAP explanations.
    - Note that there are two explanations for a single instance: there is an explanation from the baseline value for each class in the target variable.
- Part 2: 
    - Run the “register model and explainer” part.
    - The first and second cell contain the code to link to the workspace and register the model.
    - The Third cell on line 9, needs your input! This cell creates the .yml file with the necessary dependencies.
    - Run the “Deploy model” part. This cell uses a python script (line 13). This script only contains the code to deploy the model. Add the code to deploy the explainer too!
